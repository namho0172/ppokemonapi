package com.enh.pokemonapi.service;

import com.enh.pokemonapi.entity.Colosseum;
import com.enh.pokemonapi.entity.Ppokemon;
import com.enh.pokemonapi.model.colosseum.FightInPpokemonResponse;
import com.enh.pokemonapi.model.colosseum.FightPpokemonItem;
import com.enh.pokemonapi.repository.ColosseumRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor // Constructor --> 생성자 (종속성 자동주입)
public class ColosseumService {
    private final ColosseumRepository colosseumRepository;
    public void setColosseum(Ppokemon ppokemon) throws Exception{
        // 뽀켓몬 둘이상 입장이 불가능하니깐 현재 몇놀의 뽀켓몬이 결투장에 있는지 알아야 한다.
        // 결투장에 진입한 몬스터 리스트를 가져온다.
        List<Colosseum> checkList = colosseumRepository.findAll();
        // 만약 checkList의 갯수가 2놈 이상이면 던지자
        if (checkList.size() >= 2) throw new Exception();

        Colosseum colosseum = new Colosseum();
        colosseum.setPpokemon(ppokemon);
        colosseumRepository.save(colosseum);
    }
    public void delColosseumInPpokemon(long id){
        colosseumRepository.deleteById(id);
    }
    public FightInPpokemonResponse getCurrentColosseum(){ // 콜로세움의 현재 상태를 주세요
        //일단 결투장에 진입한 몬스터 리스트를 다 가져와 근데 넣을때 최대 2마리만 넣을수 있게 해놨으니깐
        //경우의 수는 0개, 1개, 2개
        List<Colosseum> checkList = colosseumRepository.findAll();

        // FightInPpokemonResponse 모양으로 무조건 줘야한다.
        FightInPpokemonResponse response = new FightInPpokemonResponse();
        // 이렇게 하면 안쪽에 칸들은 두칸 다 null
        // 두칸 다 null 이런건 0개일때 이미 처리하고 간다는 거.

        if (checkList.size() == 2){
            //리스트에서 0번쨰 요소를 가져온다
            response.setPpokemonResponse1(convertPpokemonItem(checkList.get(0)));
            response.setPpokemonResponse2(convertPpokemonItem(checkList.get(1)));
        } else if (checkList.size()==1){
            response.setPpokemonResponse1(convertPpokemonItem(checkList.get(0)));
        }

        return response;
    }
    private FightPpokemonItem convertPpokemonItem(Colosseum colosseum){ //Colosseum을 FightPpokemonItem라는 모양의 그릇으로 받아줄거에요
        FightPpokemonItem PpokemonItem = new FightPpokemonItem();
        PpokemonItem.setPpokemonColosseumId(colosseum.getId());
        PpokemonItem.setPpkemonId(colosseum.getPpokemon().getId());
        PpokemonItem.setPpokemonName(colosseum.getPpokemon().getPpokemonName());
        PpokemonItem.setPpokemonImg(colosseum.getPpokemon().getPpokemonImg());
        PpokemonItem.setElemental(colosseum.getPpokemon().getElemental());
        PpokemonItem.setHp(colosseum.getPpokemon().getHp());
        PpokemonItem.setAttack(colosseum.getPpokemon().getAttack());
        PpokemonItem.setDefense(colosseum.getPpokemon().getDefense());
        PpokemonItem.setSpeed(colosseum.getPpokemon().getSpeed());
        PpokemonItem.setRank(colosseum.getPpokemon().getRank());
        PpokemonItem.setIntroduction(colosseum.getPpokemon().getIntroduction());

        return PpokemonItem;
    }
}
