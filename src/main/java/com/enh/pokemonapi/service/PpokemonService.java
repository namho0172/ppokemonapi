package com.enh.pokemonapi.service;

import com.enh.pokemonapi.entity.Ppokemon;
import com.enh.pokemonapi.model.PpokemonItem;
import com.enh.pokemonapi.model.PpokemonRequest;
import com.enh.pokemonapi.model.PpokemonResponse;
import com.enh.pokemonapi.repository.PpokemonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PpokemonService {
    private final PpokemonRepository ppokemonRepository;

    public Ppokemon getData(long ppokemonId) {
        return ppokemonRepository.findById(ppokemonId).orElseThrow();
    }

    public void setPpokemon(PpokemonRequest request){
        Ppokemon addData = new Ppokemon();
        addData.setPpokemonName(request.getPpokemonName());
        addData.setPpokemonImg(request.getPpokemonImg());
        addData.setElemental(request.getElemental());
        addData.setHp(request.getHp());
        addData.setAttack(request.getAttack());
        addData.setDefense(request.getDefense());
        addData.setSpeed(request.getSpeed());
        addData.setRank(request.getRank());
        addData.setIntroduction(request.getIntroduction());

        ppokemonRepository.save(addData);
    }
    public List<PpokemonItem> getPpokemons(){ //Ppokemon이라는 아이템 리스트를 줄거에요
        List<Ppokemon> originList = ppokemonRepository.findAll(); // 창고지기한테 Ppokemon이라는 리스트를 갖고 오라고 시킴

        List<PpokemonItem> result = new LinkedList<>();//리시트를 쌓아놓고 끈을 reult라는 이름으로 호칭하고 명령

        for (Ppokemon ppokemon : originList){ // 향상된 포문
            PpokemonItem addItem = new PpokemonItem(); // 포켓몬 아이템을 addItem이라는 그릇을 만드고 담아내기
            addItem.setId(ppokemon.getId()); // ppokemon에서 Id데이터를 갖고옴
            addItem.setPpokemonName(ppokemon.getPpokemonName());
            addItem.setPpokemonImg(ppokemon.getPpokemonImg());
            addItem.setElemental(ppokemon.getElemental());
            addItem.setHp(ppokemon.getHp());
            addItem.setAttack(ppokemon.getAttack());
            addItem.setDefense(ppokemon.getDefense());
            addItem.setSpeed(ppokemon.getSpeed());
            addItem.setRank(ppokemon.getRank());
            addItem.setIntroduction(ppokemon.getIntroduction());

            result.add(addItem); // result라는 끈으로 묶었어요
        }
        return result;
    }

    public PpokemonResponse getPpokemon(long id){
        Ppokemon ppokemon = ppokemonRepository.findById(id).orElseThrow();

        PpokemonResponse response = new PpokemonResponse();
        response.setId(ppokemon.getId());
        response.setPpokemonName(ppokemon.getPpokemonName());
        response.setPpokemonImg(ppokemon.getPpokemonImg());
        response.setElemental(ppokemon.getElemental().getName());
        response.setHp(ppokemon.getHp());
        response.setAttack(ppokemon.getAttack());
        response.setDefense(ppokemon.getDefense());
        response.setSpeed(ppokemon.getSpeed());
        response.setRank(ppokemon.getRank());
        response.setIntroduction(ppokemon.getIntroduction());

        return response;

    }
}
