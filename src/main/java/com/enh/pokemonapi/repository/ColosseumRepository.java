package com.enh.pokemonapi.repository;

import com.enh.pokemonapi.entity.Colosseum;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ColosseumRepository extends JpaRepository<Colosseum, Long> {
}
