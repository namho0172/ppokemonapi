package com.enh.pokemonapi.repository;

import com.enh.pokemonapi.entity.Ppokemon;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PpokemonRepository extends JpaRepository<Ppokemon, Long> {
}
