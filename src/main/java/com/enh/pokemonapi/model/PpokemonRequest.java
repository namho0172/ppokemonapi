package com.enh.pokemonapi.model;

import com.enh.pokemonapi.enums.Elemental;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PpokemonRequest{
    private String ppokemonName;
    private String ppokemonImg;
    private Elemental elemental;
    private Integer hp;
    private Integer attack;
    private Integer defense;
    private Integer speed;
    private Long rank;
    private String introduction;
}
