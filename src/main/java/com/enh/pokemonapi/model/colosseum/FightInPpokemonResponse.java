package com.enh.pokemonapi.model.colosseum;

import com.enh.pokemonapi.enums.Elemental;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FightInPpokemonResponse {
    private FightInPpokemonResponse ppokemonResponse1;
    private FightInPpokemonResponse ppokemonResponse2;

}
