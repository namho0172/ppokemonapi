package com.enh.pokemonapi.entity;

import com.enh.pokemonapi.enums.Elemental;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Ppokemon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String ppokemonName;

    @Column(nullable = false, length = 50)
    private String ppokemonImg;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Elemental elemental;

    @Column(nullable = false)
    private Integer hp;

    @Column(nullable = false)
    private Integer attack;

    @Column(nullable = false)
    private Integer defense;

    @Column(nullable = false)
    private Integer speed;

    @Column(nullable = false)
    private Long rank;

    @Column(nullable = false, length = 100)
    private String introduction;
}
