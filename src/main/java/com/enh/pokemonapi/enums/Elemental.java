package com.enh.pokemonapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Elemental {
    SPACE("우주")
    ,ICE("얼음")
    ,ROCK("암석")
    ,LEGEND("전설")
    ,ESPER("에스퍼")
    ,FLOWER("꽃")
    ,FIRE("불")
    ,WATTER("물")
    ,MACHINE("기계");

    private final String name;
}
