package com.enh.pokemonapi.controller;

import com.enh.pokemonapi.model.*;
import com.enh.pokemonapi.service.PpokemonService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/ppokemon")
public class PpokemonController {
    private final PpokemonService ppokemonService;

    @PostMapping ("/new")
    public ListResult<PpokemonItem> setPpokemon(@RequestBody PpokemonRequest request){
        ppokemonService.setPpokemon(request);

        ListResult<PpokemonItem> list = new ListResult<>();

        ListResult<PpokemonItem> response = new ListResult<>();
        response.setCode(0);
        response.setMsg("성공하였습니다.");

        return response;
    }
    @GetMapping("/all")
    public ListResult<PpokemonItem> getPpokemons(){
        List<PpokemonItem> list = ppokemonService.getPpokemons();

        ListResult<PpokemonItem> response = new ListResult<>();
        response.setList(list);
        response.setTotalCount(list.size());
        response.setCode(0);
        response.setMsg("성공하였습니다.");

        return response;
    }
    @GetMapping("/detail/{id}")
    public SingleResult<PpokemonResponse> getDetail(@PathVariable long id) {
        PpokemonResponse result = ppokemonService.getPpokemon(id);

        SingleResult<PpokemonResponse> response = new SingleResult<>();
        response.setMsg("성공하였습니다");
        response.setCode(0);
        response.setData(result);

        return response;
    }
}
