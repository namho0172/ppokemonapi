package com.enh.pokemonapi.controller;

import com.enh.pokemonapi.entity.Colosseum;
import com.enh.pokemonapi.entity.Ppokemon;
import com.enh.pokemonapi.model.CommonResult;
import com.enh.pokemonapi.model.ListResult;
import com.enh.pokemonapi.model.PpokemonItem;
import com.enh.pokemonapi.model.colosseum.ColosseumRequest;
import com.enh.pokemonapi.repository.ColosseumRepository;
import com.enh.pokemonapi.service.PpokemonService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/colosseum")
public class ColosseumController {
    private final PpokemonService ppokemonService;
    private final ColosseumRepository colosseumRepository;

    @PostMapping("/new/ppokemon/{ppokemonId}")
    public CommonResult setColosseum(@PathVariable long ppokemonId,@RequestBody ColosseumRequest request){
        Ppokemon ppokemon = ppokemonService.getData(ppokemonId);

        ListResult<Colosseum> list = new ListResult<>();

        ListResult<PpokemonItem> response = new ListResult<>();
        response.setCode(0);
        response.setMsg("성공하였습니다");

        return response;
    }
}
